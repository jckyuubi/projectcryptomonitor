'use strict'

const express = require('express');
const userController = require('../controllers/controller');

const controller = new userController();

var router = express.Router();

// Routes
/**
 * @swagger
 * /createUser:
 *  post:
 *    tags:
 *      - Create User
 *    summary: Create an user in DB.
 *    description: Create a customer and register him into a Mongo DB
 *    responses:
 *      '201':
 *        description: A successful response
 *      '400':
 *        description: Invalid request
 *      '404':
 *        description: Resource not found
 *      '500':
 *        description: Internal Server Error
 *    parameters:
 *      - name: name
 *        in: query
 *        description: Name of our new user
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: lastname
 *        in: query
 *        description: Lastname of our new user
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: user
 *        in: query
 *        description: Username of our new user
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: password
 *        in: query
 *        description: Password of the user
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: favoriteCurrency
 *        in: query
 *        description: Favorite currency (eur, usd, ars)
 *        required: true
 *        schema:
 *          type: string
 *          enum: [eur, usd, ars]
 * */

router.post('/createUser', controller.createUser);

/**
 * @swagger
 * /getCurrencies:
 *  get:
 *    tags:
 *      - List All Cryptocoins
 *    summary: Get CoinGecko currencies.
 *    description: List all the coinGecko available crypto-currencies.
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Invalid request
 *      '404':
 *        description: Resource not found
 *      '500':
 *        description: Internal Server Error
 *    parameters:
 *      - name: favoriteCurrency
 *        in: query
 *        description: User favorite currency
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 * */

 router.get('/getCurrencies', controller.getGeckoCurrencies);

/**
 * @swagger
 * /addCrypto:
 *   post:
 *     tags:
 *      - Add Cryptocurrencie
 *     summary: Add a cryptocoin to the logged User.
 *     description: First, authenticate the user and password through the parameters, then, add the parameter Crypto to the user in MongoDB.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         required: true
 *         description: User, password and cryptocoin to add.
 *         schema:
 *           type: array
 *           $ref: '#/definitions/User'
 *     responses:
 *      '201':
 *        description: A successful response
 *      '400':
 *        description: Invalid request
 *      '404':
 *        description: Resource not found
 *      '500':
 *        description: Internal Server Error
 * definitions:
 *   User:
 *     properties:
 *       user:
 *         type: string
 *         example: aslangato
 *       password:
 *         type: string
 *         example: miau
 *       crypto:
 *         type: array
 *         $ref: '#/definitions/cryptocoin'
 *   cryptocoin:
 *      properties:
 *        symbol:
 *          type: string
 *          example: btc
 *        name:
 *          type: string
 *          example: Bitcoin
 *        price:
 *          type: integer
 *          example: 30756
 *        image:
 *          type: array
 *          $ref: '#/definitions/image'
 *        last_updated:
 *          type: string
 *          example: 2021-05-25T18:31:17.839Z
 *   image:
 *      properties:
 *        thumb:
 *          type: string
 *          example: https://assets.coingecko.com/coins/images/1/thumb/bitcoin.png?1547033579
 *        small:
 *          type: string
 *          example: https://assets.coingecko.com/coins/images/1/small/bitcoin.png?1547033579
 *        larg:
 *          type: string
 *          example: https://assets.coingecko.com/coins/images/1/large/bitcoin.png?1547033579
 */

router.post('/addCrypto', controller.addCrypto);

/**
 * @swagger
 * /getUserCurrencies:
 *  get:
 *    tags:
 *      - List User Stored Crypto-Currencies
 *    summary: Get User Crypto-currencies.
 *    description: List all the crypto-currencies stored by the given user.
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Invalid request
 *      '404':
 *        description: Resource not found
 *      '500':
 *        description: Internal Server Error
 *    parameters:
 *      - name: user
 *        in: query
 *        description: User favorite currency
 *        required: true
 *        example: aslangato
 *        schema:
 *          type: string
 *          format: string
 *      - name: password
 *        in: query
 *        description: User favorite currency
 *        required: true
 *        example: miau
 *        schema:
 *          type: string
 *          format: string
 * */

 router.get('/getUserCurrencies', controller.getUserCurrencies);

module.exports = router;