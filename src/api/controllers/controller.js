'use strict'

const validator = require('validator');
require('../../../common/env');
const User = require('../models/user');
const mongoose = require('mongoose');
const CoinGecko = require('coingecko-api');
const fs = require('fs');

module.exports = class Controller {

    async getUserCurrencies(req, res) {
        try {
            const controllerInstance = new Controller();
            await controllerInstance.connectMongoDB();
            // Recoger parámetros por POST
            const body = req.query;

            // Validar datos       
            const validateUser = !validator.isEmpty(body.user);
            const validatePassword = !validator.isEmpty(body.password);
            if (validateUser && validatePassword) {

                // Guardo en BD
                await User.findOne({ user: body.user, password: body.password }, (err, result) => {
                    if (err) {
                        console.log(`Error generated while consulting user stored Cryptocoins:  ${err}`);
                        return res.status(500).send({
                            status: 500,
                            message: `Error generated while consulting user stored Cryptocoins: ${err}`,
                        });
                    }
                    if (!result) {
                        console.log('LogIn failed, please introduce correct user and password.');
                        return res.status(404).send({
                            status: 404,
                            message: 'LogIn failed, please introduce correct user and password.',
                        });
                    } else {
                        if (result.crypto) {
                            return res.status(201).send({
                                status: 201,
                                message: 'User Crypto-currencies were obtained successfully.',
                                storedCoins: result.crypto,
                            });
                        } else {
                            return res.status(404).send({
                                status: 404,
                                message: 'Requested user doesnt have any crypto-currencies added.',
                            });
                        }
                    }
                });
            } else {
                console.log(`Invalid request data`);
                return res.status(400).send({
                    status: 400,
                    message: `Invalid request data`,
                });
            }
        } catch (err) {
            console.log(`Error originated in getUserCurrencies:  ${err}`);
            return res.status(500).send({
                status: 500,
                message: `Error originated in getUserCurrencies:  ${err}`,
            });
        }
    };

    async addCrypto(req, res) {
        try {
            const controllerInstance = new Controller();
            await controllerInstance.connectMongoDB();
            // Recoger parámetros por POST
            const body = req.body;

            // Validar datos       
            const validateUser = !validator.isEmpty(body.user);
            const validatePassword = !validator.isEmpty(body.password);
            if (validateUser && validatePassword) {
                let cryptoCoins = [];
                // Loguearse para agregarle la crypto al usuario
                // Primero traigo las coins ya agregadas por el usuario
                await User.findOne({ user: body.user, password: body.password }, (err, article) => {
                    if (err) {
                        console.log(`Error generated while adding Cryptocoin:  ${err}`);
                        return res.status(500).send({
                            status: 500,
                            message: `Error generated while adding Cryptocoin: ${err}`,
                        });
                    }
                    if (!article) {
                        console.log('LogIn failed, please introduce correct user and password.');
                        return res.status(404).send({
                            status: 404,
                            message: 'LogIn failed, please introduce correct user and password.',
                        });
                    } else {
                        if (article.crypto) {
                            for (let i = 0; i < article.crypto.length; i++) {
                                cryptoCoins.push(article.crypto[i]);
                            }
                        }
                    }
                });

                // A continuación le adjunto el nuevo coin
                cryptoCoins.push(body.crypto);

                // Guardo en BD
                await User.findOneAndUpdate({ user: body.user, password: body.password }, { crypto: cryptoCoins }, (err, articleUpdated) => {
                    if (err) {
                        console.log(`Error generated while adding Cryptocoin:  ${err}`);
                        return res.status(500).send({
                            status: 500,
                            message: `Error generated while adding Cryptocoin: ${err}`,
                        });
                    }
                    if (!articleUpdated) {
                        console.log('LogIn failed, please introduce correct user and password.');
                        return res.status(404).send({
                            status: 404,
                            message: 'LogIn failed, please introduce correct user and password.',
                        });
                    }
                    return res.status(201).send({
                        status: 201,
                        message: 'Crypto was added to the user successfully.',
                    });
                });
            } else {
                console.log(`Invalid request data`);
                return res.status(400).send({
                    status: 400,
                    message: `Invalid request data`,
                });
            }
        } catch (err) {
            console.log(`Error originated in addCrypto:  ${err}`);
            return res.status(500).send({
                status: 500,
                message: `Error originated in addCrypto:  ${err}`,
            });
        }
    };

    async createUser(req, res) {
        try {
            const controllerInstance = new Controller();
            await controllerInstance.connectMongoDB();

            // Recoger parámetros por POST
            const body = req.query;

            // Validar datos
            const validateName = !validator.isEmpty(body.name);            
            const validateLastname = !validator.isEmpty(body.lastname);            
            const validateUser = !validator.isEmpty(body.user);            
            const validatePassword = !validator.isEmpty(body.password);            
            const validateFavCurrency = !validator.isEmpty(body.favoriteCurrency);            

            if (validateName && validateLastname && validateUser && validatePassword && validateFavCurrency) {
                // Crear el objeto a guardar
                let user = new User();

                // Asignar valores del usuario
                user.name = body.name;
                user.lastname = body.lastname;
                user.user = body.user;
                user.password = body.password;
                user.favoriteCurrency = body.favoriteCurrency;

                // Guardar el usuario
                user.save((err, userStored) => {
                    if (err || !userStored) {
                        return res.status(500).send({
                            status: 500,
                            message: `MongoDB error originated.`,
                        });
                    }
                    return res.status(201).send({
                        status: 201,
                        message: 'User was created successfully.',
                        createdUser: user,
                    });
                });
            } else {
                console.log(`Invalid request data`);
                return res.status(400).send({
                    status: 400,
                    message: `Invalid request data`,
                });
            }
        } catch (err) {
            console.log(`Error originated in createUser:  ${err}`);
            return res.status(500).send({
                status: 500,
                message: `Error originated in createUser:  ${err}`,
            });
        }
    };

    async getGeckoCurrencies(req, res) {
        try {
            // Inicializar cliente Gecko API
            const CoinGeckoClient = new CoinGecko();

            // Realizar el llamado al cliente
            const geckoResponse = await CoinGeckoClient.coins.all({ per_page: 250, localization: false, sparkline: false });

            if (req.query.favoriteCurrency) {
                let geckoResponse2 = [];
                if (geckoResponse.code === 200) {
                    for (let i = 0; i < geckoResponse.data.length; i++) {
                        let newCoin = {
                            symbol: geckoResponse.data[i].symbol,
                            price: geckoResponse.data[i].market_data.current_price[req.query.favoriteCurrency],
                            name: geckoResponse.data[i].name,
                            image: geckoResponse.data[i].image,
                            last_updated: geckoResponse.data[i].last_updated,
                        };
                        geckoResponse2.push(newCoin);
                    }
                }
                return res.send({
                    status: 200,
                    message: 'CoinGecko crypto-currencies obtained successfully.',
                    cryptoCoinsList: geckoResponse2,
                });
            }
        } catch (err) {
            console.log(`Error originated in getCurrencies:  ${err}`);
            return res.status(500).send({
                status: 500,
                message: `Error originated in getCurrencies:  ${err}`,
            });
        }
    };

    async connectMongoDB() {
        try {
            mongoose.Promise = global.Promise;
            // await mongoose.connect(`mongodb://${process.env.MONGODB_CONN}${process.env.MONGODB_NAME}`, {
            await mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASS}@cluster0.dzxyk.mongodb.net/${process.env.MONGODB_NAME}?retryWrites=true&w=majority`, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useFindAndModify: false
            });
            console.log('MongoDB connected successfully!');
        } catch(err) {
            console.log('Failure to connect MongoDB! ', err.message);
            throw err;
        }
    };
};
