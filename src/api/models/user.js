'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = Schema({
    name: String,
    lastname: String,
    user: String,
    password: String,
    favoriteCurrency: String,
    crypto: Object,
});

module.exports = mongoose.model('User', userSchema);