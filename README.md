# ProjectCryptoMonitor

API encargada de consumir un servicio externo de la plataforma CoinGecko, obtener y mostrar su información.
Adicionalmente, consta de la posibilidad de crear usuarios, autenticarse, así como la adición de criptomonedas.

# • Installation and usage
1. Install modules
```javascript
npm i
```

2. Use npm Start in a command prompt
```javascript
npm start
```
3. Browse http://localhost:5000/docs
