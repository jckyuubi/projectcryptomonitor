const express = require('express');
const app = express();
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
// Cargar ficheros rutas
const userRouter = require('./src/api/routes/apiRouter');

const port = process.env.PORT || 5000;
const host = process.env.HOSTNAME || 'localhost';

// Extended: https://swagger.io/specification/#infoObject
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      version: '0.1.0',
      title: 'Crypto-currencies Monitor API',
      description: 'API wrappler for CoinGekko, crypto-currencies monitor developed using Express.',
      author: 'Jean Carlo Arevalo Diaz',
      contact: {
        name: 'Jean Carlo Arevalo Diaz',
        email: 'jcad1996@gmail.com',
      },
    },
    servers: [ { url: 'http://localhost:5000' } ]
  },
  apis: ['./src/api/routes/*.js']
  // apis: ['app.js']
};

const swaggerDocs = swaggerJsdoc(swaggerOptions);

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));
app.use(express.json());
app.use('/', userRouter);

app.listen(port, () => {
  console.log(`Server listening on ${host}:${port}`);
});